@base <https://purl.org/coscine/ap/isf/gasMetalArcWelding/> .

@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix sh: <http://www.w3.org/ns/shacl#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .

@prefix dcterms: <http://purl.org/dc/terms/> .

@prefix isf: <https://purl.org/coscine/terms/isf#> .
@prefix coscineISFgasMetalArcWelding: <https://purl.org/coscine/ap/isf/gasMetalArcWelding#> .

<https://purl.org/coscine/ap/isf/gasMetalArcWelding/>
  dcterms:license <http://spdx.org/licenses/MIT> ;
  dcterms:publisher <https://isf.rwth-aachen.de/> ;
  dcterms:title  "ISF gas metal arc welding"@en, "ISF Metallschutzgasschweißen"@de ;

  a sh:NodeShape ;
  sh:targetClass <https://purl.org/coscine/ap/isf/gasMetalArcWelding/> ;
  sh:closed true ;

  sh:property [
    sh:path rdf:type ;
  ] ;

  sh:property coscineISFgasMetalArcWelding:current ;
  sh:property coscineISFgasMetalArcWelding:voltage ;
  sh:property coscineISFgasMetalArcWelding:polarity ;
  sh:property coscineISFgasMetalArcWelding:weldingFillerMaterialDiameter ;
  sh:property coscineISFgasMetalArcWelding:weldingFillerMaterial ;
  sh:property coscineISFgasMetalArcWelding:wireType ;
  sh:property coscineISFgasMetalArcWelding:inertGasComposition ;
  sh:property coscineISFgasMetalArcWelding:inertGasFlow ;
  sh:property coscineISFgasMetalArcWelding:contactTubeLife ;
  sh:property coscineISFgasMetalArcWelding:wireFeedSpeed ;
  sh:property coscineISFgasMetalArcWelding:electrodeInclinationAngle ;
  sh:property coscineISFgasMetalArcWelding:powerContactTubeDistance ;
  sh:property coscineISFgasMetalArcWelding:torchOffset ;
  sh:property coscineISFgasMetalArcWelding:weldingPosition ;
  sh:property coscineISFgasMetalArcWelding:gasNozzleLife ;
  sh:property coscineISFgasMetalArcWelding:torchMovement ;
  sh:property coscineISFgasMetalArcWelding:powerSource ;
  sh:property coscineISFgasMetalArcWelding:characteristicLine ;
  sh:property coscineISFgasMetalArcWelding:arcType ;
  sh:property coscineISFgasMetalArcWelding:workpieceMaterial ;
  sh:property coscineISFgasMetalArcWelding:jointType ;
  sh:property coscineISFgasMetalArcWelding:jointPreparation ;
  sh:property coscineISFgasMetalArcWelding:backing ;
  sh:property coscineISFgasMetalArcWelding:surfaceTreatment ;
  sh:property coscineISFgasMetalArcWelding:weldingSpeed ;
  sh:property coscineISFgasMetalArcWelding:robot ;
  sh:property coscineISFgasMetalArcWelding:preheating ;
  sh:property coscineISFgasMetalArcWelding:expectedArcBlow ;
  sh:property coscineISFgasMetalArcWelding:externalMagneticField ;
  sh:property coscineISFgasMetalArcWelding:videoRecordings ;
  sh:property coscineISFgasMetalArcWelding:framesPerSecound ;
  sh:property coscineISFgasMetalArcWelding:aperture ;
  sh:property coscineISFgasMetalArcWelding:exposureTime ;
  sh:property coscineISFgasMetalArcWelding:cameraName ;
  sh:property coscineISFgasMetalArcWelding:pyrometerRecordings ;
  sh:property coscineISFgasMetalArcWelding:crossSection ;
  sh:property coscineISFgasMetalArcWelding:microStructureExamination .

coscineISFgasMetalArcWelding:current
  sh:path isf:current ;
  sh:order 0 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Current"@en, "Stromstärke"@de .

coscineISFgasMetalArcWelding:voltage
  sh:path isf:voltage ;
  sh:order 1 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Voltage"@en, "Spannung"@de .

coscineISFgasMetalArcWelding:polarity
  sh:path isf:polarity ;
  sh:order 2 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Polarity"@en, "Polung"@de ;
  sh:class <http://purl.org/coscine/vocabularies/isf/polarity> .

coscineISFgasMetalArcWelding:weldingFillerMaterialDiameter
  sh:path isf:weldingFillerMaterialDiameter ;
  sh:order 3 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Wire diameter"@en, "Drahtdurchmesser"@de .

coscineISFgasMetalArcWelding:weldingFillerMaterial
  sh:path isf:weldingFillerMaterial ;
  sh:order 4 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Wire Material"@en, "Drahtmaterial"@de .

coscineISFgasMetalArcWelding:wireType
  sh:path isf:wireType ;
  sh:order 5 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Wire type"@en, "Drahtart"@de ;
  sh:class <http://purl.org/coscine/vocabularies/isf/wireType> .

coscineISFgasMetalArcWelding:inertGasComposition
  sh:path isf:inertGasComposition ;
  sh:order 6 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Inert gas composition"@en, "Schutzgaszusammensetzung"@de .

coscineISFgasMetalArcWelding:inertGasFlow
  sh:path isf:inertGasFlow ;
  sh:order 7 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Inert gas flow"@en, "Schutzgasstrom"@de .

coscineISFgasMetalArcWelding:contactTubeLife
  sh:path isf:contactTubeLife ;
  sh:order 8 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Contact tube life"@en, "Kontaktrohr Standzeit"@de .

coscineISFgasMetalArcWelding:wireFeedSpeed
  sh:path isf:wireFeedSpeed ;
  sh:order 9 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Wire feed speed"@en, "Drahtvorschubgeschwindigkeit"@de .

coscineISFgasMetalArcWelding:electrodeInclinationAngle
  sh:path isf:electrodeInclinationAngle ;
  sh:order 10 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Torch angle"@en, "Brennerwinkel"@de .

coscineISFgasMetalArcWelding:powerContactTubeDistance
  sh:path isf:powerContactTubeDistance ;
  sh:order 11 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Contact tube distance"@en, "Kontaktrohrabstand"@de .

coscineISFgasMetalArcWelding:torchOffset
  sh:path isf:torchOffset ;
  sh:order 12 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Torch Offset"@en, "Brenner Versatz"@de .

coscineISFgasMetalArcWelding:weldingPosition
  sh:path isf:weldingPosition ;
  sh:order 13 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Welding position"@en, "Schweißposition"@de ;
  sh:class <http://purl.org/coscine/vocabularies/isf/weldingPosition> .

coscineISFgasMetalArcWelding:gasNozzleLife
  sh:path isf:gasNozzleLife ;
  sh:order 14 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Gas nozzle life"@en, "Gasdüse Standzeit"@de .

coscineISFgasMetalArcWelding:torchMovement
  sh:path isf:torchMovement ;
  sh:order 15 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Torch movement"@en, "Brennerbewegung"@de .

coscineISFgasMetalArcWelding:powerSource
  sh:path isf:powerSource ;
  sh:order 16 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Power source"@en, "Stromquelle"@de .

coscineISFgasMetalArcWelding:characteristicLine
  sh:path isf:characteristicLine ;
  sh:order 17 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Synergy characteristic line"@en, "Synergiekennlinie"@de .

coscineISFgasMetalArcWelding:arcType
  sh:path isf:arcType ;
  sh:order 18 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Arc type"@en, "Lichtbogenart"@de ;
  sh:class <http://purl.org/coscine/vocabularies/isf/arcType> .

coscineISFgasMetalArcWelding:workpieceMaterial
  sh:path isf:workpieceMaterial ;
  sh:order 19 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Workpiece material"@en, "Werkstückmaterial"@de .

coscineISFgasMetalArcWelding:jointType
  sh:path isf:jointType ;
  sh:order 20 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Joint type"@en, "Stoßart"@de ;
  sh:class <http://purl.org/coscine/vocabularies/isf/jointType> .

coscineISFgasMetalArcWelding:jointPreparation
  sh:path isf:jointPreparation ;
  sh:order 21 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Joint Preparation"@en, "Nahtvorbereitung"@de .

coscineISFgasMetalArcWelding:backing
  sh:path isf:backing ;
  sh:order 22 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:boolean ;
  sh:name "Backing"@en, "Badsicherung"@de .

coscineISFgasMetalArcWelding:surfaceTreatment
  sh:path isf:surfaceTreatment ;
  sh:order 23 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:name "Surface treatment"@en, "Oberflächenbehandlung"@de ;
  sh:class <http://purl.org/coscine/vocabularies/isf/surfaceTreatment> .

coscineISFgasMetalArcWelding:weldingSpeed
  sh:path isf:weldingSpeed ;
  sh:order 24 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Welding speed"@en, "Schweißgeschwindigkeit"@de .

coscineISFgasMetalArcWelding:robot
  sh:path isf:robot ;
  sh:order 25 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:boolean ;
  sh:name "Robot"@en, "Roboter"@de .

coscineISFgasMetalArcWelding:preheating
  sh:path isf:preheating ;
  sh:order 26 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Preheating"@en, "Vorwärmung"@de .

coscineISFgasMetalArcWelding:expectedArcBlow
  sh:path isf:expectedArcBlow ;
  sh:order 27 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:boolean ;
  sh:name "Expected arc blow?"@en, "Blaswirkung erwartet"@de .

coscineISFgasMetalArcWelding:externalMagneticField
  sh:path isf:externalMagneticField ;
  sh:order 28 ;
  sh:minCount 1 ;
  sh:maxCount 1 ;
  sh:datatype xsd:boolean ;
  sh:name "External magnetic field"@en, "Externes Magnetfeld"@de .

## MSG Kamera Parameter

coscineISFgasMetalArcWelding:videoRecordings
  sh:path isf:videoRecordings ;
  sh:order 29 ;
  sh:maxCount 1 ;
  sh:name "Video recordings"@en, "Videoaufnahmen"@de ;
  sh:class <http://purl.org/coscine/vocabularies/isf/videoRecordings> .

coscineISFgasMetalArcWelding:framesPerSecound
  sh:path isf:framesPerSecound ;
  sh:order 30 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Frames per secound"@en, "Bilder pro Sekunde"@de .

coscineISFgasMetalArcWelding:aperture
  sh:path isf:aperture ;
  sh:order 31 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Aperture"@en, "Blende"@de .

coscineISFgasMetalArcWelding:exposureTime
  sh:path isf:exposureTime ;
  sh:order 32 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Exposure time"@en, "Belichtungszeit"@de .

coscineISFgasMetalArcWelding:cameraName
  sh:path isf:cameraName ;
  sh:order 33 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Camera name"@en, "Kamerabezeichnung"@de .

coscineISFgasMetalArcWelding:pyrometerRecordings
  sh:path isf:pyrometerRecordings ;
  sh:order 34 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Pyrometer Recordings"@en, "Pyrometeraufnahmen"@de .

coscineISFgasMetalArcWelding:crossSection
  sh:path isf:crossSection ;
  sh:order 35 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Cross section"@en, "Schliff"@de .

coscineISFgasMetalArcWelding:microStructureExamination
  sh:path isf:microStructureExamination ;
  sh:order 36 ;
  sh:maxCount 1 ;
  sh:datatype xsd:string ;
  sh:name "Micro structure examination"@en, "Mikrostrukturuntersuchung"@de .
